function getPrice(){
    var obs={
        ukulele:{base:4500},
        acoustic:{
            base:10000,
            radios:{
                rad1:7000,
                rad2:5000,
                rad3:35000
            }
        },
        electric:{
            base: 15000,
            radios:{
                rad1:600,
                rad2:7000,
                rad3:50
            }
        },
        bas:{
            base:20000,
            check_opt:7000
        }
    };

    let res=document.getElementById("res");
    let amount=document.getElementById("amount");
    let select=document.getElementById("selecter");

    let radio=document.getElementsByName("myradio");
    let s_radio = 0;
    if (radio[0].checked){
        s_radio=obs.acoustic.radios.rad1;
    }
    if (radio[1].checked){
        s_radio=obs.acoustic.radios.rad2;
    }
    if (radio[2].checked){
        s_radio=obs.acoustic.radios.rad3;
    }

    let abil1=document.getElementsByName("myradio1");
    let s_abil = 0;
    if (abil1[0].checked){
        s_abil +=obs.electric.radios.rad1;
    }
    let abil2=document.getElementsByName("myradio2");
    if (abil2[0].checked){
        s_abil +=obs.electric.radios.rad2;
    }
    let abil3=document.getElementsByName("myradio3");
    if (abil3[0].checked){
        s_abil +=obs.electric.radios.rad3;
    }

    let checker=document.getElementById("calc");
    let s_check = 0;
    if(checker.checked){
        s_check=obs.bas.check_opt;
    }

    if (select.value === "1") {
        res.innerHTML=amount.value*(obs.acoustic.base+s_radio);
    }
    else if (select.value === "2"){
        res.innerHTML=amount.value*obs.electric.base+s_abil;
    }
    else if (select.value === "3"){
        res.innerHTML=amount.value*obs.bas.base+s_check;
    }
    else if (select.value === "4"){
        res.innerHTML=amount.value*obs.ukulele.base;
    }
}



window.addEventListener("DOMContentLoaded", function(event){
    let a=document.getElementById("calculator");
    a.addEventListener("change", function(event){
        let b=document.getElementById("res");
        b.innerHTML=0;
        let amount=document.getElementById("amount");
        let select=document.getElementById("selecter");
        let radio=document.getElementById("radios");
        let abil=document.getElementById("abilities");
        let checker=document.getElementById("checker");
        if (amount.value === ""){
            radio.style.display="none";
            checker.style.display="none";
            abil.style.display="none";
            select.style.display="disabled selected value";
            b.innerHTML=0;
        } else if (!(/^\d+$/).test(amount.value)){
            alert("Введите корректное значение!");
            amount.value="";
            radio.style.display="none";
            checker.style.display="none";
            abil.style.display="none";
            select.style.display="disabled selected value";
            b.innerHTML=0;
        } else {
            if (select.value==="4"){
            radio.style.display="none";
            checker.style.display="none";
            abil.style.display="none";
            } 
            else if (select.value==="1"){
            radio.style.display="block";
            checker.style.display="none";
            abil.style.display="none";
            }
            else if (select.value==="2"){
            radio.style.display="none";
            checker.style.display="none";
            abil.style.display="block";    
            }
            else if (select.value==="3"){
             radio.style.display="none";
            checker.style.display="block";
            abil.style.display="none";
            }
            getPrice(select.value);
        }
    });
});